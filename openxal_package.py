#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This script downloads all artifacts from Artifactory, including Open XAL core,
extensions, plugins, lattice files, and applications. It can also create a
tarball.

Created on Mon Jul 22 13:43:01 2019

@author: Juan F. Esteban Müller <JuanF.EstebanMuller@esss.se>
"""


import argparse
import json
import os
import sys
import tarfile
import urllib.request
import urllib3

urllib3.disable_warnings()

ARTIFACTORY_REPO = "https://artifactory.esss.lu.se/artifactory/"
REPO_NAME = "OpenXAL/"
API_STRING = "api/storage/"
GROUP_ID = "org/xal/"

GITLAB_API = "https://gitlab.esss.lu.se/api/v4/projects"
LATTICE_PROJECT = "openxal-lattice"
JELOG_PROJECT = "jelog"


def get_latest_version_list(http, url, gitlab_lattice_url, gitlab_jelog_url):
    # Getting the list of artifacts from Artifactory
    request = http.request("GET", url)
    artifacts_json = json.loads(request.data.decode("utf-8"))
    if "errors" in artifacts_json:
        raise Exception("Not a valid URL")
    artifacts_list = [child["uri"][1:] for child in artifacts_json["children"]]

    # For each artifact, find the latest version
    versions_dict = {}
    for artifact in artifacts_list:
        request = http.request("GET", url + artifact)
        versions_json = json.loads(request.data.decode("utf-8"))

        # Get all versions and remove trailing "-SNAPSHOT" (if present).
        versions_list = [child["uri"][1:] for child in versions_json["children"]]
        versions_int_list = [tuple(map(int, (child["uri"][1:].split("-")[0].split(".")))) for child in versions_json["children"]]

        if versions_list != []:
            latest_version = versions_list[versions_int_list.index(max(versions_int_list))]
        else:
            latest_version = None

        versions_dict[artifact] = {"version": latest_version, "ver_type": ["release/" if url.find("snapshot") == -1 else "snapshot/"][0]}

    # Getting latest version for lattice
    request = http.request("GET", gitlab_lattice_url + "/repository/tags")
    tags_json = json.loads(request.data.decode("utf-8"))
    # Finding latest version
    versions_list = [tag["name"] for tag in tags_json]
    versions_int_list = [tuple(map(int, (tag["name"].split("-")[0].split(".")))) for tag in tags_json]
    latest_version = versions_list[versions_int_list.index(max(versions_int_list))]

    versions_dict["optics"] = {"version": latest_version, "ver_type": None}

    # Getting latest version for lattice
    request = http.request("GET", gitlab_jelog_url + "/repository/tags")
    tags_json = json.loads(request.data.decode("utf-8"))
    # Finding latest version
    versions_list = [tag["name"] for tag in tags_json]
    versions_int_list = [tuple(map(int, (tag["name"].split("-")[0].split(".")))) for tag in tags_json]
    latest_version = versions_list[versions_int_list.index(max(versions_int_list))]

    versions_dict["jelog"] = {"version": latest_version, "ver_type": None}

    return versions_dict


def get_version_list_from_file(filename):
    versions_dict = {}
    versions_file = open(filename, "r")
    for line in versions_file:
        line = line.split("#")[0]
        line_split = line.split("=")
        if len(line_split) == 2:
            versions_dict[line_split[0]] = {
                "version": line_split[1].strip(),
                "ver_type": ["release/" if line_split[1].find("SNAPSHOT") == -1 else "snapshot/"][0],
            }
    versions_file.close()

    return versions_dict


__name__ = "__main__"
args = {"command": "name", "path": ".", "version": "latest-stable"}
if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="Open XAL install utility. If run with no arguments, \
            it downloads the latest stable release."
    )
    parser.add_argument(
        "command",
        nargs="?",
        default="install",
        choices=["install", "package"],
        help='command to execute. "install" downloads all artifacts.\
            "package" runs "install" and then generates a tarball.',
    )
    parser.add_argument("-p", "--path", action="store", help="path to install Open XAL. Default is working directory.")
    parser.add_argument(
        "-v",
        "--version",
        default="latest-stable",
        help='Open XAL and applications version. Can be "latest-stable", \
            "latest-snapshot", a json string containing artifact names and \
            version numbers, or a file giving the file containing a list \
            artifacts and versions, one per line and separated by an = sign. \
            Use "optics" for the lattice version.',
    )

    # Try to parse the arguments.
    try:
        args = parser.parse_args()
    except argparse.ArgumentError:
        sys.exit()

    if args.path is not None:
        path = args.path
    else:
        path = "."

    http = urllib3.PoolManager()

    # Find lattice repository
    request = http.request("GET", GITLAB_API + "?search=" + LATTICE_PROJECT)
    projects_json = json.loads(request.data.decode("utf-8"))
    for project in projects_json:
        if project["name"] == LATTICE_PROJECT:
            project_id = project["id"]
    GITLAB_LATTICE_URL = GITLAB_API + "/" + str(project_id)

    # Find jelog repository
    request = http.request("GET", GITLAB_API + "?search=" + JELOG_PROJECT)
    projects_json = json.loads(request.data.decode("utf-8"))
    for project in projects_json:
        if project["name"] == JELOG_PROJECT:
            project_id = project["id"]
    GITLAB_JELOG_URL = GITLAB_API + "/" + str(project_id)

    # Executing install command:
    if args.version == "latest-stable":
        versions_dict = get_latest_version_list(
            http, ARTIFACTORY_REPO + API_STRING + REPO_NAME + "release/" + GROUP_ID, GITLAB_LATTICE_URL, GITLAB_JELOG_URL
        )
    elif args.version == "latest-snapshot":
        versions_dict = get_latest_version_list(
            http, ARTIFACTORY_REPO + API_STRING + REPO_NAME + "snapshot/" + GROUP_ID, GITLAB_LATTICE_URL, GITLAB_JELOG_URL
        )
    else:
        if os.path.exists(args.version):
            versions_dict = get_version_list_from_file(args.version)
        else:
            versions_dict = json.loads(args.version)
            for artifact in versions_dict.keys():
                version = versions_dict[artifact]
                versions_dict[artifact] = {
                    "version": version.strip(),
                    "ver_type": ["release/" if version.find("SNAPSHOT") == -1 else "snapshot/"][0],
                }

    # Check required artifacts.
    artifact_dist = "openxal.dist"
    if artifact_dist not in versions_dict.keys():
        print("Artifact's version list does not contains openxal.dist.", "Please add the required version for this artifact.")
        sys.exit()

    if "optics" not in versions_dict.keys():
        print(
            "Artifact's version list does not contains optics.",
            "If this was not intentional, please add the required version for this artifact.",
        )

    # Downloading openxal.dist and unpacking it.
    try:
        main_tar = urllib.request.urlretrieve(
            ARTIFACTORY_REPO
            + REPO_NAME
            + versions_dict[artifact_dist]["ver_type"]
            + GROUP_ID
            + artifact_dist
            + "/"
            + versions_dict[artifact_dist]["version"]
            + "/"
            + artifact_dist
            + "-"
            + versions_dict[artifact_dist]["version"]
            + "-dist.tar.gz"
        )
    except urllib.error.HTTPError:
        print("Couldn't download " + artifact_dist + ". Please check the URL and version.")
        sys.exit()

    tar = tarfile.open(main_tar[0])
    tar.extractall(path=path)
    tar.close()

    OXAL_DIR = path + "/openxal-" + versions_dict[artifact_dist]["version"]

    # Downloading apps.
    APPS_DIR = OXAL_DIR + "/apps/"
    if not os.path.exists(APPS_DIR):
        os.makedirs(APPS_DIR)
    for artifact in versions_dict:
        if artifact.find("app.") != -1 and versions_dict[artifact]["version"] is not None:
            try:
                urllib.request.urlretrieve(
                    ARTIFACTORY_REPO
                    + REPO_NAME
                    + versions_dict[artifact]["ver_type"]
                    + GROUP_ID
                    + artifact
                    + "/"
                    + versions_dict[artifact]["version"]
                    + "/"
                    + artifact
                    + "-"
                    + versions_dict[artifact]["version"]
                    + ".jar",
                    APPS_DIR + artifact.split(".")[-1] + "-" + versions_dict[artifact]["version"] + ".jar",
                )
            except urllib.error.HTTPError:
                print("Couldn't download " + artifact + ". Please check the URL and version.")

    try:
        versions_dict["optics"]
        # Downloading optics files.
        OPTICS_DIR = OXAL_DIR + "/optics/"
        try:
            optics_tar = urllib.request.urlretrieve(GITLAB_LATTICE_URL + "/repository/archive?sha=" + versions_dict["optics"]["version"])
        except urllib.error.HTTPError:
            print("Couldn't download lattice files.", "Please check the URL and version.")
            sys.exit()
        tar = tarfile.open(optics_tar[0])
        root_dir = tar.getnames()[0]
        files = tar.getmembers()
        # Removing Readme file
        for file_i in files:
            if file_i.name == root_dir + "/README.md":
                files.remove(file_i)
        # Moving files to optics directory
        for file_i in files:
            file_i.path = "optics" + file_i.path[len(root_dir) :]
        # Extracting the files
        tar.extractall(members=files, path=path + "/openxal-" + versions_dict[artifact_dist]["version"])
        tar.close()
    except Exception:
        print("Optics files NOT downloaded.")

    try:
        versions_dict["jelog"]
        # Downloading CKEditor files from Jelog repo.
        CKEDITOR_DIR = OXAL_DIR + "/lib/"
        try:
            jelog_tar = urllib.request.urlretrieve(GITLAB_JELOG_URL + "/repository/archive?sha=" + versions_dict["jelog"]["version"])
        except urllib.error.HTTPError:
            print("Couldn't download CKEditor files.", "Please check the URL and version.")
            sys.exit()
        tar = tarfile.open(jelog_tar[0])
        root_dir = tar.getnames()[0]
        files = tar.getmembers()
        # Removing unnecessary files and directories
        newFiles = []
        for file_i in files:
            if file_i.name.startswith(root_dir + "/src/html"):
                newFiles.append(file_i)
        # Moving files to optics directory
        for file_i in newFiles:
            file_i.path = "lib" + file_i.path[len(root_dir + "/src") :]
        # Extracting the files
        tar.extractall(members=newFiles, path=path + "/openxal-" + versions_dict[artifact_dist]["version"])
        tar.close()
    except Exception:
        print("CKEditor files NOT downloaded.")

    if args.command == "package" or args.command == "deploy":
        print("Generating tarball...")
        with tarfile.open("openxal-" + versions_dict[artifact_dist]["version"] + ".tar.gz", "w:gz") as tar:
            tar.add(
                path + "/openxal-" + versions_dict[artifact_dist]["version"], arcname="openxal-" + versions_dict[artifact_dist]["version"]
            )

    print("Done.")
