# Open XAL package

This Python script automatically downloads Open XAL main jar files, lattice, and applications.

## Usage
```
usage: openxal_package.py [-h] [-p PATH] [-v VERSION] [{install,package}]

Open XAL install utility. If run with no arguments, it downloads the latest
stable release.

positional arguments:
  {install,package}     command to execute. "install" downloads all artifacts.
                        "package" runs "install" and then generates a tarball.

optional arguments:
  -h, --help            show this help message and exit
  -p PATH, --path PATH  path to install Open XAL. Default is working
                        directory.
  -v VERSION, --version VERSION
                        Open XAL and applications version. Can be "latest-
                        stable", "latest-snapshot", or a file giving the file
                        containing a list artifacts and versions, one per line
                        and separated by an = sign. Use "optics" for the
                        lattice version.
```

## Version list file structure
The file should contain only one artifact per line. The version must be separated from the artifact name bby an '=' sign. One can mix stable releases and snapshots.
There are 2 artifacts that are required: openxal.dist, which contains Open XAL main jars; and optics, which contains the lattice. Application artifacts are optional, and the script will only download the applications listed in the file.
Line comments are allowed preceeded by #.

### Example:
```
# Open XAL core
openxal.dist=2.0
# Lattice files
optics=1.0
# Applications
openxal.app.launcher=2.0 # Stable application
openxal.app.lebt=1.3.1-SNAPSHOT # Testing a development version
```
